<?php
	if ($connected==1) {

 	include 'connexion.php';

?>
<div class=" content-area">
	<div class="page-header">
		<h4 class="page-title">Dashboard</h4>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Accueil</a></li>
			<li class="breadcrumb-item active" aria-current="page">Dashboard </li>
		</ol>
	</div>

	<div class="row row-cards">
		<div class="col-sm-12 col-lg-6 col-xl-3 col-md-6">
			<div class="card card-img-holder text-default bg-color">
				<div class="row">
					<div class="col-4">
						<div class="circle-icon bg-primary text-center align-self-center shadow-primary"><img src="assets\images\circle.svg" class="card-img-absolute"><i class="lnr lnr-user fs-30  text-white mt-4"></i></div>
					</div>
					<div class="col-8">
						<?php
				          $req_select=$bdd->prepare('SELECT * FROM accords');
				          $req_select->execute(array('sect'=>$section));
				          $n=$req_select->rowCount();
			            ?>
						<div class="card-body p-4">
							<h1 class="mb-3"><?=$n?></h1>
							<h5 class="font-weight-normal mb-0">Nombre d'accords</h5>
						</div>
					</div>
				</div>
		   </div>
		</div>

	</div>
</div>
<?php
}
else{
	include 'login.php';
}
?>