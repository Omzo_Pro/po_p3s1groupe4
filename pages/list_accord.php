<div class=" content-area">
	<div class="page-header">
		<h1 class="page-title">Accords</h1>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
			<li class="breadcrumb-item active" aria-current="page">Liste des Accords</li>
		</ol>
	</div>
	<div class="row row-cards">
		
		<div class="col-lg-12 col-xl-9">
			<div class="card"> 
			    <div class="card-header">
				    <h2 class="card-title">Liste des Accords</h2>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<?php
						if (isset($erreur)) {
							echo $erreur;
						}
						?>
						<table id="example" class="table table-striped table-bordered border-top-0 border-bottom-0" style="width:100%">
					    	<thead>
						        <tr class="border-bottom-0">
						        	<th class="wd-45p">Titre</th>
						        	<th class="wd-30p">conventionnaire</th>
						        	<th class="wd-10p">date signé</th>
						        	<th class="wd-5p"></th>
						        </tr>
					     	</thead>
					    	<tbody>
					    		<?php
					    			$req_select=$bdd->prepare('SELECT * FROM accords');
					    			$req_select->execute(array());

					                while($donnees=$req_select->fetch(PDO::FETCH_ASSOC)){
					                    $titre=$donnees['titre'];
					                    $conventionnaire=$donnees['conventionnaire'];
					                    $date=$donnees['date'];
					                    $texte=$donnees['texte'];
					                    $fichier=$donnees['fichier'];
					                    $ID=$donnees['ID'];
					    		?>
								<tr>
									<td><?=$titre?></td>
									<td><?=$conventionnaire?></td>
									<td><?=$date?></td>
									<td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalLong<?php echo $matricule ?>"><i class="fa fa-eye"></i></button></td>
								</tr>

								<!--Scrolling Modal-->
								<div class="modal fade" id="exampleModalLong<?php echo $matricule ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle"><?=$titre?></h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="">

													<div class="card-body text-center">
						
														<p class="mb-0"><span class="text-black"><?=$texte?><</span></p>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<?php
									}
								?>
							</tbody>  
					    
					    </table>
					</div>
				</div>
				
				
			</div>
			
		</div>
	</div>
</div>