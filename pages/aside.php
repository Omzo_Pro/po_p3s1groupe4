<?php
if ($connected==1) {
?>
<!-- Sidebar Holder -->
<nav id="sidebar" class="nav-sidebar">
	<ul class="list-unstyled components" id="accordion">
		<div class="user-profile">
			<div class="dropdown user-pro-body">
				<div><img src="assets\images\<?=$profile?>" alt="user-img" class="img-circle"></div>
				<div class="mb-2"><a href="#" class="" data-toggle="" aria-haspopup="true" aria-expanded="false"> <span class="font-weight-semibold"><?=$prenom?> <?=$nom?></span>  </a>
				<br><span class="text-gray">Etudiant</span>
				</div>
			</div>
		</div>

		<li>
			<a href="?p=accueil" class=" wave-effect"><i class="fa fa-desktop mr-2"></i> Accueil</a>
		</li>
		<li class="">
			<a href="#secretaires" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
				<i class="fa fa-crosshairs mr-2"></i>Accords
			</a>
			<ul class="collapse list-unstyled" id="secretaires" data-parent="#accordion">
				<li>
					<a href="?p=list-accord">Liste des accords</a>
				</li>
			</ul>
		</li>

	</ul>
</nav>
<?php
}
?>