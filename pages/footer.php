			<?php
			  if ($connected==1) {
			?>
				</div>
			</div>

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2021 <a href="#">ProDev</a>. Designed by <a href="#">Prodev</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>
		<?php
		}
		?>
		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>

		<!-- Dashboard Core -->
		<script src="assets\js\vendors\jquery-3.2.1.min.js"></script>
		<script src="assets\js\vendors\bootstrap.bundle.min.js"></script>
		<script src="assets\js\vendors\jquery.sparkline.min.js"></script>
		<script src="assets\js\vendors\selectize.min.js"></script>
		<script src="assets\js\vendors\jquery.tablesorter.min.js"></script>
		<script src="assets\js\vendors\circle-progress.min.js"></script>
		<script src="assets\plugins\rating\jquery.rating-stars.js"></script>

		<!-- Fullside-menu Js-->
		<script src="assets\plugins\fullside-menu\jquery.slimscroll.min.js"></script>
		<script src="assets\plugins\fullside-menu\waves.min.js"></script>

		<!-- Dashboard Core -->
		<script src="assets\js\index1.js"></script>

		<!--Morris.js Charts Plugin -->
		<script src="assets\plugins\morris\raphael-min.js"></script>
		<script src="assets\plugins\morris\morris.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="assets\plugins\scroll-bar\jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- Custom Js-->
		<script src="assets\js\custom.js"></script>

	</body>
</html>