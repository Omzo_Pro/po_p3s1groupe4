<?php 
  session_start();
  include('connexion.php');
  include('session.php');
  include('scrypt.php');


  $connected=0;
  $erreur_admin_connexion=0;

  //$activation=1;
  if(isset($_POST['admin_connexion'])){
    
  	$login=$_POST['login'];    
    $pass=$_POST['password'];

    $password=sha1(sha1($pass));

    $req=$bdd->prepare('SELECT * FROM membres WHERE email=:login AND password=:password AND activation=:activation');
    $req->execute(array('login'=>$login,'password'=>$password,'activation'=>1)) or die(print_r($req->errorInfo()));
      
    $n1=$req->rowCount();

    if(($n1==0)){
      $connected=0;
      $erreur='
        <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert"
              aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <i class="mdi mdi-block-helper"></i>
          <strong>Oups!</strong><br/> Login et/ou mot de passe incorrect.
        </div>';
    }
    else{
      $connected=1;
      $_SESSION['admin_connexion']=array('login'=>$login);
    } 
  }
  
  if(isset($_SESSION['admin_connexion'])){

    $infos=$_SESSION['admin_connexion'];
    $login=$infos['login'];
    $connected=1;
  }

  if(isset($_GET['admin_deconnexion'])){
    unset($_SESSION['admin_connexion']);
    
    $connected=0;
    header('location:index.php');
    exit();
  }
      
?>
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

		<!-- Title -->
		<title>CA</title>
		<link rel="stylesheet" href="assets\fonts\fonts\font-awesome.min.css">

		<!-- Sidemenu Css -->
		<link href="assets\plugins\fullside-menu\css\style.css" rel="stylesheet">
		<link href="assets\plugins\fullside-menu\waves.min.css" rel="stylesheet">

		<!-- Dashboard Css -->
		<link href="assets\css\dashboard.css" rel="stylesheet">

		<!-- Morris.js Charts Plugin -->
		<link href="assets\plugins\morris\morris.css" rel="stylesheet">

		<!-- Custom scroll bar css-->
		<link href="assets\plugins\scroll-bar\jquery.mCustomScrollbar.css" rel="stylesheet">

		<!---Font icons-->
		<link href="assets\css\icons.css" rel="stylesheet">
	</head>
	<body class="">
		<div class="page">
			<div class="page-main">
			<?php
			if ($connected==1) {
			  include 'session.php';
			?>
				<div class="app-header1 header py-1 d-flex">
					<div class="container-fluid">
						<div class="d-flex">
							<a class="header-brand" href="?p=accueil">
								<img src="assets\images\brand\logo.png" class="header-brand-img" alt="CA">
							</a>
							<div class="menu-toggle-button">
								<a class="nav-link wave-effect" href="#" id="sidebarCollapse">
									<span class="fa fa-bars"></span>
								</a>
							</div>
							<div class="d-flex order-lg-2 ml-auto header-right-icons header-search-icon">
								<div class="p-2">
									<form class="input-icon ">
										<div class="input-icon-addon">
											<i class="fe fe-search"></i>
										</div>
										<input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
									</form>
								</div>

								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon full-screen-link nav-link-bg">
										<i class="fa fa-expand" id="fullscreen-button"></i>
									</a>
								</div>
								<div class="dropdown text-center selector">
									<a href="#" class="nav-link leading-none" data-toggle="dropdown">
										<span class="avatar avatar-sm brround cover-image" data-image-src="assets/images/<?=$profile?>"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">
										<div class="text-center">
											<a href="#" class="dropdown-item text-center font-weight-sembold user" data-toggle="dropdown">Omar Ndiaye</a>
											<span class="text-center user-semi-title text-dark">Etudiant</span>
											<div class="dropdown-divider"></div>
										</div>
										<a class="dropdown-item" href="?admin_deconnexion=true">
											<i class="dropdown-icon mdi  mdi-logout-variant"></i> Deconnexion
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wrapper">
			<?php include("aside.php") ?>
			<?php
			}
			?>

