<?php 
	include('connexion.php');
	$erreur='';
	$date=date('Y-m-d H:i:s');

//Ajouter un membre
	if(isset($_POST['add_membre'])){
		$erreur='';
		$prenom=$_POST['prenom'];
		$nom=$_POST['nom'];
		$genre=$_POST['genre'];
		$date_naiss=$_POST['date_naiss'];
		$telphone=$_POST['telephone'];
		$adresse=$_POST['adresse'];
		$profession=$_POST['profession'];
		$nin=$_POST['nin'];
		$secretaire=$_POST['matricule'];
		$section=$_POST['section'];

		$secret=rand(0,9999999);

		$profile=addslashes($_FILES['profile']['name']);
		$size=$_FILES['profile']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($profile,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM membres WHERE NIN=:nin');
		$req_select->execute(array('nin'=>$nin));
		$n=$req_select->rowCount();
		
		if($n==0)
		{
			if(isset($image))
			{
				if (in_array($extenson_image, $extensionimg_valides))
				{
					$dossier= 'assets/images/membre/';
					$upload_image=$_FILES['profile']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$profile);

					if($size < 2048000)
					{	

						$req_insert=$bdd->prepare('INSERT INTO membres (idMembre, prenom, nom, profile, sexe, date_naissance, adresse, telephone, profession, NIN, matricule, section, sacretaire, date_registred) VALUES (:id, :prnm, :nom, :profil, :sex, :datnais, :adrs, :phone, :job, :nin, :mat, :sctn, :secretaire :dat)');
						$req_insert->execute(array(
									'id'=>NULL,
									'prnm'=>$prenom,
									'nom'=>$nom,
									'profil'=>$image,
									'sex'=>$genre,
									'datnais'=>$date_naiss,
									'adrs'=>$adresse,
									'phone'=>$telephone,
									'job'=>$profession,
									'nin'=>$nin,
									'mat'=>$secret,
									'sctn'=>$section,
									'secretaire'=>$secretaire,
									'dat'=>$date
								));
			
			
						if($req_insert)
						{
						$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Membre ajouté avec succés!</div>';
						}
						else{
						$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!</div>';
						}
					}
					else{
					$erreur="image trop grand";
					}
				
				}
				else{
						$erreur="image non valide";
					}
					
			}else{
				$image="default.jpg";
			}
			
				
		}
		else{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Erreur:</strong>Membre déja enregistré(e).</div>';
			}
	}

//ajouter une section

	if(isset($_POST['add_section']))
	{
    	$erreur='';
    	$section = $_POST['section'];
    	$siege = $_POST['siege'];
    	$secret=rand(0,9999999);
    	$matricule=$section.$secret;

		$req_insert_rubrique=$bdd->prepare('INSERT INTO sections (ID, section, siege, matricule) VALUES (:id, :sec, :siege, :scrt)');
		$req_insert_rubrique->execute(array(
						'id'=>NULL,
						'sec'=>$section,
						'siege'=>$siege,
						'scrt'=>$matricule));
		if ($req_insert_rubrique)
		{
			$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Section ajoutée avec succés!</div>';
		}
		else
		{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Echec d\'ajout. Réessayer!</div>';
		}

	}

//Update Section
	if(isset($_POST['edit_section'])){
		$erreur='';
		
		$section=$_POST['section'];
		$siege=$_POST['siege'];
		$matricule=$_POST['matricule'];
		
		
		$req_select=$bdd->prepare('SELECT * FROM sections WHERE matricule=:sec');
		$req_select->execute(array('sec'=>$matricule));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
	
		if($n>0)
		{
			$req_update_=$bdd->prepare('UPDATE sections SET section=:sec, siege=:siege WHERE matricule=:sct');
			$req_update_->execute(array(
						'sec'=>$section,
						'siege'=>$siege,
						'sct'=>$matricule));
			
			if ($req_update_) 
			{
				$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Mise à jour de la section réussie!</div>';
			}
			else
			{
				$erreur='
						<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-block-helper"></i>
							<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
						</div>';
			}
		}
					
		else
		{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Aucun rubrique trouvé. Réessayer!</div>';
		}
	}

//Ajout de secretaire
if(isset($_POST['add_secretaire'])){

		$erreur='';
		$prenom=$_POST['prenom'];
		$nom=$_POST['nom'];
		$section=$_POST['section'];
		$genre=$_POST['genre'];
		$login=$_POST['login'];
		$password=$_POST['password'];
		$password2=$_POST['password2'];
		$phone=$_POST['telephone'];
		$adresse=$_POST['adresse'];
		$fonction="admin";
		$photo="default.png";

		$secret=rand(0,9999999);
		
		
		$req_select=$bdd->prepare('SELECT * FROM secretaires WHERE login=:log');
		$req_select->execute(array('log'=>$login));
		$n=$req_select->rowCount();
		
		if($n==0)
		{
			if($password==$password2)
			{

				$crypted_pwd=sha1(sha1($password));
		
				$req_insert=$bdd->prepare('INSERT INTO secretaires (ID, fonction, prenom, nom, profile, login, password, sexe, adresse, telephone, section, activation, matricule) VALUES (:id, :fct, :prenom, :nom, :photo, :mail, :pass, :sex, :adrs, :phone, :sctn, :act, :secret)');
				$req_insert->execute(array(
										'id'=>NULL,
										'fct'=>$fonction,
										'prenom'=>$prenom,
										'nom'=>$nom,
										'photo'=>$photo,
										'mail'=>$login,
										'pass'=>$crypted_pwd,
										'sex'=>$genre,
										'adrs'=>$adresse,
										'phone'=>$phone,
										'sctn'=>$section,
										'act'=>1,
										'secret'=>$secret
									));
				
				
				if($req_insert)
					{
					$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Secetaire ajouté(e) avec succés!</div>';
					}
					else{
					$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!</div>';
					}
			
				
			}
			else{
				$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Erreur:</strong> Votre mot de passe et la confirmation de mot passe ne correspond pas</div>';
				}
		}
		else{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Erreur:</strong> Cet login est déjà associé à un compte.</div>';
			}
	}
//Update Admin
	if(isset($_POST['update_admin'])){
		$erreur='';
		$email=$_POST['email'];
		$nom=$_POST['nom'];
		$telephone=$_POST['telephone'];
		$prenom=$_POST['prenom'];
		$secret=$_POST['secret'];
		
		
		$profile=addslashes($_FILES['profile']['name']);
		$size=$_FILES['profile']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($profile,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM administrateurs WHERE secret=:sec');
		$req_select->execute(array('sec'=>$secret));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
		if($n>0){
			
			if(!empty($profile)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= '../images/membre/';
						$upload_image=$_FILES['profile']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$profile);
							
					}
				}
			}
			else
			{
				$profile=$donnees['profile'];
			}
			
			$req_update_=$bdd->prepare('UPDATE administrateurs SET profile=:photo, nom=:nom, prenom=:pren, telephone=:tel,  email=:mail WHERE secret=:sec');
			$req_update_->execute(array(
									'photo'=>$profile,
									'nom'=>$nom,
									'pren'=>$prenom,
									'tel'=>$telephone,
									'mail'=>$email,
									'sec'=>$secret
								));
			
			
			if($req_update_){
				$erreur='
					<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Mise à jour du membre réussie!</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
					</div>';
			}
				
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun membre n\'est trouvé.
				</div>';
		}
	}

//Delete Admin

	if(isset($_POST['delete_admin']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM administrateurs WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong> suppréssion réussie</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}

//Modification mot de passe-

	if (isset($_POST['edit_password'])){
	
		$secret=$_POST['secret'];
		$password=$_POST['password'];
		$password1=$_POST['pass'];
		$new_password=$_POST['pwd_news'];
		$confirme_password=$_POST['conf_pwd'];
		
		$pass=sha1(sha1($password1));	
          //puis on teste si le mot de passe et le confirmation de mot de passe sont identique
			if($pass==$password){
				//Enfin on teste si le mot de passe est supÃ©rieur au 6 caracteres
				if(strlen($password1)>=6){
					if($new_password==$confirme_password){
				
				$new_pass=sha1(sha1($new_password));	

				$req_new=$bdd->prepare('UPDATE administrateurs set password=:password where secret=:secret');
					$req_new->execute(array(
					'secret'=>$secret,
					'password'=>$new_pass
					)); if($req_new){
							 $erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>Modification de mot de passe réussie</strong> 
								</div>';
							 }
							 else
							 	{
								 $erreur='erreur ';
								}
			
				}
				else{
					$erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>Votre mot de passe et  la confirmation de passe ne correspond pas </strong> 
								</div>';
					}
				}
				else
					{
					$erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>Votre nouveau mot de passe doit dépasser au moins 6 caracters </strong> 
								</div>';
					}
				}
				else
					{
					$erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>l\'ancien mot de passe est incorrect..</strong> 
								</div> ';
					}
				
				}



//ajouter un rubrique

	if(isset($_POST['add_rubrique']))
	{
    	$erreur='';
    	$category = $_POST['rubrique'];
    	$secret=rand(0,9999999);

		$req_insert_rubrique=$bdd->prepare('INSERT INTO categories (idCat, category, matricule) VALUES (:id, :cat, :scrt)');
		$req_insert_rubrique->execute(array(
						'id'=>NULL,
						'cat'=>$category,
						'scrt'=>$secret));
		if ($req_insert_rubrique)
		{
			$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Rubrique publiée avec succés!</div>';
		}
		else
		{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Echec d\'ajout. Réessayer!</div>';
		}

	}

//Update rubrique
	if(isset($_POST['edit_rubrique'])){
		$erreur='';
		
		$category=$_POST['rubrique'];
		$matricule=$_POST['matricule'];
		
		
		$req_select=$bdd->prepare('SELECT * FROM categories WHERE matricule=:sec');
		$req_select->execute(array('sec'=>$matricule));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
	
		if($n>0)
		{
			$req_update_=$bdd->prepare('UPDATE categories SET category=:cat WHERE matricule=:sct');
			$req_update_->execute(array(
						'cat'=>$category,
						'sct'=>$matricule));
			
			if ($req_update_) 
			{
				$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Mise à jour du rubrique réussie!</div>';
			}
			else
			{
				$erreur='
						<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-block-helper"></i>
							<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
						</div>';
			}
		}
					
		else
		{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Aucun rubrique trouvé. Réessayer!</div>';
		}
	}
	
//Delete rubrique
	if(isset($_POST['delete_rubrique']))
	{
			
		$erreur='';
		$matricule=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('DELETE FROM categories WHERE matricule=:mat');
		$req_supp->execute(array('mat'=>$matricule));
							
			if($req_supp){
				$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Rubrique supprimer avec succés!</div>';
			}
			else{
				$erreur=
					'<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Echec de la suppression. Réessayer!</div>';
			}
	}

//ajouter une Article d'image
	if(isset($_POST['add_article_image']))
	{
    	$erreur='';
    	$type = "image";
    	$publicateur = 451251;
    	$tags = $_POST['tags'];
    	$une = $_POST['une'];
    	if (empty($une)) {
    		$une="off";
    	}
    	$category = $_POST['category'];
    	$titre = $_POST['titre'];
    	$texte=htmlspecialchars($_POST['texte']);
    	$matricule=rand(0,9999999);

		$image=($_FILES['image']['name']);
		$size=$_FILES['image']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
			if(isset($image))
			{
				if (in_array($extenson_image, $extensionimg_valides))
				{
					$dossier= '../assets/images/articles/images/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						$req_insert_article=$bdd->prepare('INSERT INTO articles (idArt, titre, category, type, texte, tags, publicateur, a_la_une, date_ajout,matricule, image) VALUES (:id, :titr, :cat, :typ, :txt, :tag, :pub, :une, :dat, :mat, :img)');
						$req_insert_article->execute(array(
										'id'=>NULL,
										'titr'=>$titre,
										'cat'=>$category,
										'typ'=>$type,
										'txt'=>$texte,
										'tag'=>$tags,
										'pub'=>$publicateur,
										'une'=>$une,
										'dat'=>$date,
										'mat'=>$matricule,
										'img'=>$image));
						if ($req_insert_article) 
						{
							$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Annonce ajouté avec succes!</div>';
						}
						else
						{
							$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Echec d\'ajout. Réessayer!</div>';
						}
					}
					else
						{
							$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: image trop grand. Réessayer!</div>';
						}
				}
				else
					{
						$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: image non valide. Réessayer!</div>';
					}
			}
			else
			{
			$image='default.jpg';
			}
	}

//ajouter une Article video
	if(isset($_POST['add_article_video']))
	{
    	$erreur='';
    	$type = "video";
    	$publicateur = 451251;
    	$video = $_POST['video'];
    	$tags = $_POST['tags'];
    	$une = $_POST['une'];
    	if (empty($une)) {
    		$une="off";
    	}
    	$category = $_POST['category'];
    	$titre = $_POST['titre'];
    	$texte=htmlspecialchars($_POST['texte']);
    	$matricule=rand(0,9999999);
		
		$req_insert_article=$bdd->prepare('INSERT INTO articles (idArt, titre, category, type, texte, tags, publicateur, a_la_une, date_ajout, matricule, video) VALUES (:id, :titr, :cat, :typ, :txt, :tag, :pub, :une, :dat, :mat, :vdo)');
		$req_insert_article->execute(array(
						'id'=>NULL,
						'titr'=>$titre,
						'cat'=>$category,
						'typ'=>$type,
						'txt'=>$texte,
						'tag'=>$tags,
						'pub'=>$publicateur,
						'une'=>$une,
						'dat'=>$date,
						'mat'=>$matricule,
						'vdo'=>$video));
		if ($req_insert_article) 
		{
			$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Annonce ajouté avec succes!</div>';
		}
		else
		{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Echec d\'ajout. Réessayer!</div>';
		}
	}				
		
//ajouter une Article d'images sliders
	if (isset($_POST['add_article_slides']))
	
	{
		$erreur='';
    	$type = "slides";
    	$publicateur = 451251;
    	$tags = $_POST['tags'];
    	$une = $_POST['une'];
    	if (empty($une)) {
    		$une="off";
    	}
    	$category = $_POST['category'];
    	$titre = $_POST['titre'];
    	$texte=htmlspecialchars($_POST['texte']);
    	$matricule=rand(0,9999999);

		$image1=($_FILES['image1']['name']);
		$size=$_FILES['image1']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image1,'.'),1));

		$image2=($_FILES['image2']['name']);
		$size=$_FILES['image2']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image2,'.'),1));

		$image3=($_FILES['image3']['name']);
		$size=$_FILES['image3']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image3,'.'),1));
		
		if(isset($image1) AND isset($image2) OR isset($image3))
		{
				if($size<2048000)
				{
					$dossier= '../assets/images/articles/slides/';

					$upload_image1=$_FILES['image1']['tmp_name'];
					move_uploaded_file($upload_image1,$dossier.$image1);

					$upload_image2=$_FILES['image2']['tmp_name'];
					move_uploaded_file($upload_image2,$dossier.$image2);

					$upload_image3=$_FILES['image3']['tmp_name'];
					move_uploaded_file($upload_image3,$dossier.$image3);
		

					$req_insert_article=$bdd->prepare('INSERT INTO articles (idArt, titre, category, type, texte, tags, publicateur, a_la_une, date_ajout,matricule, slider1, slider2, slider3) VALUES (:id, :titr, :cat, :typ, :txt, :tag, :pub, :une, :dat, :mat, :img1, :img2, :img3)');
						$req_insert_article->execute(array(
										'id'=>NULL,
										'titr'=>$titre,
										'cat'=>$category,
										'typ'=>$type,
										'txt'=>$texte,
										'tag'=>$tags,
										'pub'=>$publicateur,
										'une'=>$une,
										'dat'=>$date,
										'mat'=>$matricule,
										'img1'=>$image1,
										'img2'=>$image2,
										'img3'=>$image3));
						if ($req_insert_article) 
						{
							$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Annonce ajouté avec succes!</div>';
						}
						else
						{
							$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>echec d\'ajout!</div>';
						}
				}
				else
				{
					$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Oups image trop grand!</div>';
				}
			
		}
		else
		{
			
			$image1='default.jpg';
			$image2='default.jpg';
			$image3='default.jpg';
		}

	}

//Update Article
	if(isset($_POST['edit_article'])){
		$erreur='';
		
		$category=$_POST['category'];
		$matricule=$_POST['matricule'];
		$tags=$_POST['tags'];
        $texte=htmlspecialchars($_POST['texte']);
        $titre=$_POST['titre'];
		
		
		$req_select=$bdd->prepare('SELECT * FROM articles WHERE matricule=:sec');
		$req_select->execute(array('sec'=>$matricule));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
	
		if($n>0)
		{
			$req_update_=$bdd->prepare('UPDATE articles SET category=:cat, titre=:titr, texte=:txte, tags=:tag WHERE matricule=:sct');
			$req_update_->execute(array(
						'cat'=>$category,
						'titr'=>$titre,
						'txte'=>$texte,
						'tag'=>$tags,
						'sct'=>$matricule));
			
			if ($req_update_) 
			{
				$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Mise à jour de l\'article réussie!</div>';
			}
			else
			{
				$erreur='
						<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-block-helper"></i>
							<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
						</div>';
			}
		}
					
		else
		{
			$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Aucun article trouvé. Réessayer!</div>';
		}
	}

//Delete article
	if(isset($_POST['delete_article']))
	{
			
		$erreur='';
		$matricule=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('DELETE FROM articles WHERE matricule=:mat');
		$req_supp->execute(array('mat'=>$matricule));
							
			if($req_supp){
				$erreur='<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Article supprimé avec succés!</div>';
			}
			else{
				$erreur=
					'<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Erreur: Echec de la suppression. Réessayer!</div>';
			}
	}

//Publier Article
	if(isset($_POST['pub_article']))
	{
			
		$erreur='';
		$secret=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('UPDATE articles set publication=:pub, date_publication=:dat WHERE matricule=:sec');
		$req_supp->execute(array('sec'=>$secret, 'dat'=>$date, 'pub'=>1));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Article publiée avec succés!</div>';
			}
			else{
				$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Echec de la publication. Réessayer!</div>';
			}
	}

//DisPublier Article
	if(isset($_POST['dispub_article']))
	{
			
		$erreur='';
		$secret=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('UPDATE articles set publication=:pub WHERE matricule=:sec');
		$req_supp->execute(array('sec'=>$secret, 'pub'=>0));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Publication enlevé avec succés!</div>';
			}
			else{
				$erreur='<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Echec. Réessayer!</div>';
			}
	}

// Update information de OAKO
	if(isset($_POST['update_info'])){

		$erreur='';
		$id=$_POST['id'];
		$presentation=$_POST['presentation'];
		$telephone1=$_POST['telephone1'];
		$telephone2=$_POST['telephone2'];
		$telephone3=$_POST['telephone3'];
		$email=$_POST['email'];
		$adresse=$_POST['adresse'];
		$face=$_POST['facebook'];
		$instagram=$_POST['instagram'];
		$linkedin=$_POST['linkedin'];
		$twitter=$_POST['twitter'];
		$siteweb=$_POST['siteweb'];
		$objectif=stripslashes($_POST['objectif']);
		$slogan=stripslashes($_POST['slogan']);
		$complements=stripslashes($_POST['complement']);


		$req_update_=$bdd->prepare('UPDATE contact SET presentation=:presentation, telephone1=:telephone1, telephone2=:telephone2, telephone3=:telephone3, adresse=:adresse, email=:email,facebook=:facebook,twitter=:twitter,linkedin=:linkedin, instagram=:instagram, siteweb=:siteweb, objectif=:objectif,slogan=:slogan,complements=:complements  WHERE ID=:id');
			$req_update_->execute(array(
									'id'=>$id,
									'presentation'=>$presentation,
									'telephone1'=>$telephone1,
									'telephone2'=>$telephone2,
									'telephone3'=>$telephone3,
									'adresse'=>$adresse,
									'email'=>$email,
									'facebook'=>$face,
									'twitter'=>$twitter,
									'linkedin'=>$linkedin,
									'instagram'=>$instagram,
									'siteweb'=>$siteweb,
									'objectif'=>$objectif,
									'slogan'=>$slogan,
									'complements'=>$complements
								));
			
			
			if($req_update_){
				$erreur='
					<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Mise à jour des informations réussie! </strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de la mise à jour. Veuillez réessayer !!!
					</div>';
			}
				
		
	}








//ajouter un partenaire
	if(isset($_POST['ajouterpartenaire']))
	{
    	$erreur='';
    	$nomPartenaire = $_POST['nom'];
		$logoPartenaire=($_FILES['image']['name']);
		$size=$_FILES['image']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($logoPartenaire,'.'),1));
		
			if(isset($logoPartenaire))
			{
				if (in_array($extenson_image, $extensionimg_valides))
				{
					$dossier= 'images/partenaires/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$logoPartenaire);

					if($size < 2048000)
					{	
						$req_insert_video=$bdd->prepare('INSERT INTO partenaires (idPartenaire, nomPartenaire, logoPartenaire) VALUES (:id, :nm, :img)');
						$req_insert_video->execute(array(
										'id'=>NULL,
										'nm'=>$nomPartenaire,
										'img'=>$logoPartenaire));
						if ($req_insert_video) 
						{
							$erreur="partenaire ajouté avec succes";
						}
						else
						{
							$erreur="echec d'ajout";
						}
					}
					else
						{
							$erreur="image trop grand";
						}
				}
				else
					{
						$erreur="image non valide";
					}
			}
			else
			{
			$logoPartenaire='default.jpg';
			}
		
	}

//ajouter une annonce
	if(isset($_POST['add-annonce']))
	{
    	$erreur='';
    	$titre = $_POST['titre'];
    	$source = $_POST['source'];
    	$secteur = $_POST['secteur'];
    	$annonce = $_POST['annonce'];
    	$secret=rand(0,9999999);

		$image=($_FILES['image']['name']);
		$size=$_FILES['image']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
			if(isset($image))
			{
				if (in_array($extenson_image, $extensionimg_valides))
				{
					$dossier= '../images/annonces/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						$req_insert_article=$bdd->prepare('INSERT INTO annonces (idAnnonce, titre, secteur, source, annonce, date_posted,secret, image) VALUES (:id, :titr, :sec, :src, :txt, :dat, :scrt, :img)');
						$req_insert_article->execute(array(
										'id'=>NULL,
										'titr'=>$titre,
										'sec'=>$secteur,
										'src'=>$source,
										'txt'=>$annonce,
										'dat'=>$date,
										'scrt'=>$secret,
										'img'=>$image));
						if ($req_insert_article) 
						{
							$erreur="Annonce ajouté avec succes";
						}
						else
						{
							$erreur="echec d'ajout";
						}
					}
					else
						{
							$erreur="image trop grand";
						}
				}
				else
					{
						$erreur="image non valide";
					}
			}
			else
			{
			$fond='default.jpg';
			}
		
	}

//Update annonce
	if(isset($_POST['update_annonce'])){
		$erreur='';
		if(isset($_POST['titre'])){
			$titre=$_POST['titre'];
		}else{
			$titre='';
		}
		$titre=$_POST['titre'];
		$secret=$_POST['secret'];
		$titre=$_POST['titre'];
		$source=$_POST['source'];
		$fr_desc=$_POST['annonce'];
		$secteur=$_POST['secteur'];
		
		$image=$_FILES['image']['name'];
		$size=$_FILES['image']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM annonces WHERE secret=:sec');
		$req_select->execute(array('sec'=>$secret));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
		if($n>0)
		{
			if(isset($image))
			{
				if (in_array($extenson_image, $extension_valides))
				{
					$dossier= '../images/annonces/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						
						$req_update_=$bdd->prepare('UPDATE annonces SET titre=:titre, secteur=:sec, annonce=:fr_desc, source=:src, image=:image, date_posted=:dat WHERE secret=:sct');
						$req_update_->execute(array(
												'titre'=>$titre,
												'sec'=>$secteur,
												'fr_desc'=>$fr_desc,
												'src'=>$source,
												'image'=>$image,
												'dat'=>$date,
												'sct'=>$secret
											));
						if ($req_update_) 
						{
							$erreur='<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<i class="mdi mdi-check-all"></i>
										<strong>Mise à jour du de l\'annonce réussie!</strong>
									</div>';
						}
						else
						{
							$erreur='
									<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<i class="mdi mdi-block-helper"></i>
										<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
									</div>';
						}
					}
					else
						{
							$erreur='<span style="color: red; font-size: 20px">image trop grand</span>';
						}
				}
				else
					{
						$erreur='<span style="color: red; font-size: 20px">image trop grand</span>';
					}
			}
			else
			{
			$image='default.jpg';
			}
		}	
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun produit trouvé.
				</div>';
		}
	}


//Delete Annonce
	if(isset($_POST['delete_annonce']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM annonces WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Annonce supprimer avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}

//ajouter un Partenaire
	if(isset($_POST['add_partenaire']))
	{
    	$erreur='';
    	$nomPartenaire = $_POST['nomPartenaire'];
    	$adresse = $_POST['adresse'];
    	$description = $_POST['description'];
    	$contact = $_POST['contact'];
    	$secret=rand(0,9999999);

		$image=($_FILES['image']['name']);
		$size=$_FILES['image']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
			if(isset($image))
			{
				if (in_array($extenson_image, $extensionimg_valides))
				{
					$dossier= '../images/partenaires/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						$req_insert_article=$bdd->prepare('INSERT INTO partenaires (idPartenaire, nomPartenaire, descriptionPartenaire, contactPartenaire, adressePartenaire, secret, logoPartenaire) VALUES (:id, :nm, :des, :ctc, :adr, :scrt, :img)');
						$req_insert_article->execute(array(
										'id'=>NULL,
										'nm'=>$nomPartenaire,
										'des'=>$description,
										'ctc'=>$contact,
										'adr'=>$adresse,
										'scrt'=>$secret,
										'img'=>$image));
						if ($req_insert_article) 
						{
							$erreur="Partenaire ajouté avec succes";
						}
						else
						{
							$erreur="echec d'ajout";
						}
					}
					else
						{
							$erreur="image trop grand";
						}
				}
				else
					{
						$erreur="image non valide";
					}
			}
			else
			{
			$logoPartenaire='default.jpg';
			}
		
	}

//Update Partenaire
	if(isset($_POST['update_partenaire'])){
		$erreur='';
		
		$nomPartenaire=$_POST['nom'];
		$secret=$_POST['secret'];
		$adresse=$_POST['adresse'];
		$description=$_POST['description'];
		$contact=$_POST['contact'];
		
		$image=$_FILES['image']['name'];
		$size=$_FILES['image']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM partenaires WHERE secret=:sec');
		$req_select->execute(array('sec'=>$secret));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
		if($n>0)
		{
			if(isset($image))
			{
				if (in_array($extenson_image, $extension_valides))
				{
					$dossier= '../images/partenaires/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						$req_update_=$bdd->prepare('UPDATE partenaires SET nomPartenaire=:nomPartenaire, contactPartenaire=:sec, descriptionPartenaire=:description, adressePartenaire=:asr, logoPartenaire=:image WHERE secret=:sct');
						$req_update_->execute(array(
									'nomPartenaire'=>$nomPartenaire,
									'sec'=>$contact,
									'description'=>$description,
									'asr'=>$adresse,
									'image'=>$image,
									'sct'=>$secret
								));
						if ($req_update_) 
						{
							$erreur='<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<i class="mdi mdi-check-all"></i>
										<strong>Mise à jour du partenaire réussie!</strong>
									</div>';
						}
						else
						{
							$erreur='
									<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<i class="mdi mdi-block-helper"></i>
										<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
									</div>';
						}
					}
					else
						{
							$erreur='<span style="color: red; font-size: 20px">image trop grand</span>';
						}
				}
				else
					{
						$erreur='<span style="color: red; font-size: 20px">image trop grand</span>';
					}
			}
			else
			{
			$logoPartenaire='default.jpg';
			}
		}	
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun produit trouvé.
				</div>';
		}
	}

//Delete Partenaire
	if(isset($_POST['delete_partenaire']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM partenaires WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Partenaire supprimer avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}


// Update images geosen
	if(isset($_POST['update_images'])){
		$erreur='';
		$id=$_POST['id'];
		$pdg=$_POST['pdg'];
		$developpeur=$_POST['developpeur'];
		$telephone1=$_POST['telephone1'];
		$telephone2=$_POST['telephone2'];
		$telephone3=$_POST['telephone3'];
		$email=$_POST['email'];
		$adresse=$_POST['adresse'];
		$face=$_POST['facebook'];
		$instagram=$_POST['instagram'];
		$tube=$_POST['youtube'];
		$twitter=$_POST['twitter'];
		$siteweb=$_POST['siteweb'];
		$objectif=stripslashes($_POST['objectif']);
		$slogan=stripslashes($_POST['slogan']);
		$complements=stripslashes($_POST['complements']);

		$profilpdg=$_FILES['profilpdg']['name'];
		$size=$_FILES['profilpdg']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($profilpdg,'.'),1));
		
		$profildev=$_FILES['profildev']['name'];
		$size=$_FILES['profildev']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($profildev,'.'),1));

		$req_select=$bdd->prepare('SELECT * FROM oako WHERE id=:id');
		$req_select->execute(array('id'=>$id));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);

		if(!empty($profilpdg)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= '../images/membre';
						$upload_image=$_FILES['profilpdg']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$profilpdg);
							
					}
				}
			}
			else
			{
				$profilpdg=$donnees['profilpdg'];
			}
		if(!empty($profildev)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= '../images/membre';
						$upload_image=$_FILES['profildev']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$profildev);
							
					}
				}
			}
			else
			{
				$profildev=$donnees['profildev'];
			}
			
			$req_update_=$bdd->prepare('UPDATE oako SET pdg=:pdg, developpeur=:developpeur, profilpdg=:profilpdg, profildev=:profildev, telephone1=:telephone1, telephone2=:telephone2, telephone3=:telephone3, adresse=:adresse, email=:email,facebook=:facebook,twitter=:twitter,youtube=:youtube, siteweb=:siteweb, objectif=:objectif,slogan=:slogan,complements=:complements  WHERE ID=:id');
			$req_update_->execute(array(
									'id'=>$id,
									'pdg'=>$pdg,
									'developpeur'=>$developpeur,
									'profilpdg'=>$profilpdg,
									'profildev'=>$profildev,
									'telephone1'=>$telephone1,
									'telephone2'=>$telephone2,
									'telephone3'=>$telephone3,
									'adresse'=>$adresse,
									'email'=>$email,
									'facebook'=>$face,
									'twitter'=>$twitter,
									'youtube'=>$tube,
									'siteweb'=>$siteweb,
									'objectif'=>$objectif,
									'slogan'=>$slogan,
									'complements'=>$complements
								));
			
			
			if($req_update_){
				$erreur='
					<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Mise à jour des informations réussie! </strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de la mise à jour. Veuillez réessayer !!!
					</div>';
			}
				
		
	}

//Desactiver Membre

	if(isset($_POST['desactive_membre']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
		 $req_select=$bdd->prepare('SELECT * FROM membres WHERE secret=:secret');
        $req_select->execute(array('secret'=>$secret));
        $donnees=$req_select->fetch(PDO::FETCH_ASSOC);
        $activation=$donnees['activation']; 
        if($activation=='0'){
           $activation=1;
        }else{
        	$activation=0;
        }
		$req_supp=$bdd->prepare('UPDATE  membres SET activation=:activation WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret,
			                      'activation'=>$activation
			                       ));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Statut du membre modifié avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la modification de la statut. Réessayer!.
					</div>';
			}
	}
	


	
	


	//----------------envoi un mail -------------------------

	if (isset($_POST['envoyer_mail'])){
	
		$objet= $_POST['objet'];
		$email_src= $_POST['email_src'];
		$email_dest= $_POST['email_dest'];
		$message_mail= $_POST['message'];
		$secret=rand(0,66666);
		$destinataire=$email_dest;
	    $type='envoi';
		$lecture=0;
		// Pour les champs $expediteur / $copie / $destinataire, sÃ©parer par une virgule s'il y a plusieurs adresses
		$message=$message_mail;
		
		$headers = 'From: '.$email_src.'' . "\r\n" .
			 'Reply-To: '.$email_src.'' . "\r\n" .
			 'X-Mailer: PHP/' . phpversion();
			 
		if (mail($destinataire, $objet, $message_mail, $headers)) // Envoi du message
		{
			$erreur_mail='Votre message a été bien envoyé';
			
			$req_insert=$bdd->prepare('INSERT INTO messagerie (	ID,email,name,objet,message,secret,type,lecture,date) VALUES (:ID,:email,:name,:objet,:message,:secret,:type,:lecture,:date)');
		    $req_insert->execute(array(
								'ID'=>NULL,
								'email'=>$email_dest,
								'name'=>'',
								'objet'=>$objet,
								'message'=>$message_mail,
								'secret'=>$secret,
								'type'=>$type,
								'lecture'=>$lecture,
								'date'=>$date
							));
		}
		else // Non envoyÃ©
		{
			$erreur_mail= 'Votre message n\'a pas pu etre envoyé';
		}
										
	}


//ajouter un page

	if(isset($_POST['add_page']))
	{
    	$erreur='';
    	$page = $_POST['page'];
    	$secret=rand(0,9999999);

		$image=($_FILES['image']['name']);
		$size=$_FILES['image']['size'];
		$extensionimg_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
			if(isset($image))
			{
				if (in_array($extenson_image, $extensionimg_valides))
				{
					$dossier='../images/page/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						$req_insert_secteur=$bdd->prepare('INSERT INTO pages (id, page, image, secret) VALUES (:id, :sec, :img, :scrt)');
						$req_insert_secteur->execute(array(
										'id'=>NULL,
										'sec'=>$page,
										'img'=>$image,
										'scrt'=>$secret));
						if ($req_insert_secteur) 
						{
							$erreur="Page ajouté avec succes";
						}
						else
						{
							$erreur="echec d'ajout";
						}
					}
					else
						{
							$erreur="image trop grand";
						}
				}
				else
					{
						$erreur="image non valide";
					}
			}
			else
			{
			$image='default.jpg';
			}	
	}

//Update page
	if(isset($_POST['update_page'])){
		$erreur='';
		
		$page=$_POST['page'];
		$autre=$_POST['autre'];
		$secret=$_POST['secret'];
		
		$image=$_FILES['image']['name'];
		$size=$_FILES['image']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($image,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM pages WHERE secret=:sec');
		$req_select->execute(array('sec'=>$secret));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
	
		if($n>0)
		{
			if(isset($image))
			{
				if (in_array($extenson_image, $extension_valides))
				{
					$dossier= '../images/page/';
					$upload_image=$_FILES['image']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$image);

					if($size < 2048000)
					{	
						$req_update_=$bdd->prepare('UPDATE pages SET page=:page, autre=:autre, image=:img WHERE secret=:sct');
						$req_update_->execute(array(
									'page'=>$page,
									'autre'=>$autre,
									'img'=>$image,
									'sct'=>$secret));
						
						if ($req_update_) 
						{
							$erreur='<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<i class="mdi mdi-check-all"></i>
										<strong>Mise à jour de la page réussie!</strong>
									</div>';
						}
						else
						{
							$erreur='
									<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<i class="mdi mdi-block-helper"></i>
										<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
									</div>';
						}
					}
					else
						{
							$erreur='<span style="color: red; font-size: 20px">image trop grand</span>';
						}
				}
				else
					{
						$erreur='<span style="color: red; font-size: 20px">image trop grand</span>';
					}
			}
			else
			{
			$image='default.jpg';
			}
		}	
		else
		{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun produit trouvé.
				</div>';
		}
	}
	
//Delete page
	if(isset($_POST['delete_page']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM pages WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>page supprimer avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	
	
	





	
	

	

//----------------Ajouter un tshirt ----------------------------------------

	if(isset($_POST['register_tshirt'])){
		$erreur='';
        $id_article=$_POST['id_article'];
		$color=$_POST['color'];
	    $secret=rand(0,885544);
		
		$face=addslashes($_FILES['face']['name']);
		$size=$_FILES['face']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($face,'.'),1));
		
		if(!empty($face)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= '../img/tshirt/';
					$upload_image=$_FILES['face']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$face);
						
				}
			}
		}
		else
		{
			$face='';
		}
		
	    $dos=addslashes($_FILES['dos']['name']);
		$size=$_FILES['dos']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($dos,'.'),1));
		
		if(!empty($dos)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= '../img/tshirt/';
					$upload_image=$_FILES['dos']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$dos);
						
				}
			}
		}
		else
		{
			$dos='';
		}
	    $req_update_art=$bdd->prepare('UPDATE article SET id_color=:id_color WHERE ID=:id_article');
		$req_update_art->execute(array(
										'id_color'=>$color,
										'id_article'=>$id_article
										));
									   
		$req_insert=$bdd->prepare('INSERT INTO tshirt (ID, face, dos, id_color, id_article, secret) VALUES (:ID, :face, :dos, :id_color, :id_article, :secret)');
		$req_insert->execute(array(
								'ID'=>NULL,
								'face'=>$face,
								'dos'=>$dos,
								'id_color'=>$color,
								'id_article'=>$id_article,
								'secret'=>$secret
							));
		
		
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>T-shirt ajoutée avec succés! </strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de l\'ajout. Veuillez réessayer !!!
				</div>';
		}
	}
	
//----------------Modifier un tshirt ----------------------------------------

	if(isset($_POST['edit_tshirt'])){
		$erreur='';
		if(isset($_POST['id_article'])){
			$id_article=$_POST['id_article'];
		}else{
			$id_article='';
		}
		if(isset($_POST['color'])){
			$color=$_POST['color'];
		}else{
			$color='';
		}
	    $secret=$_POST['secret'];
		
		$face1=$_POST['face'];
		$dos1=$_POST['dos'];
		
		$face=$_FILES['face']['name'];
		$size=$_FILES['face']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($face,'.'),1));
		
		$dos=$_FILES['dos']['name'];
		$size_=$_FILES['dos']['size'];
		$extension_valides_= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image_= strtolower(substr(strrchr($dos,'.'),1));
		
	    $req_select=$bdd->prepare('SELECT * FROM tshirt WHERE secret=:sec');
		$req_select->execute(array('sec'=>$secret));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
		if($n>0){
			//------face----------------
			if(!empty($face)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= '../img/tshirt/';
						$upload_image=$_FILES['face']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$face);
							
					}
				}
			}
			else
			{
				$face=$face1;
			}
			//------dos----------------
			if(!empty($dos)){
				if($size_>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image_, $extension_valides_)){
						$dossier_= '../img/tshirt/';
						$upload_image_=$_FILES['dos']['tmp_name'];
						move_uploaded_file($upload_image_,$dossier_.$dos);
							
					}
				}
			}
			else
			{
				$dos=$dos1;
			}
			 $req_update_=$bdd->prepare('UPDATE article SET id_color=:id_color WHERE ID=:id_article');
		     $req_update_->execute(array(
										'id_color'=>$color,
										'id_article'=>$id_article
										));
										
			$req_update_art=$bdd->prepare('UPDATE tshirt SET face=:face, dos=:dos, id_color=:id_color, id_article=:id_article WHERE secret=:secret');
			$req_update_art->execute(array(
										'face'=>$face,
										'dos'=>$dos,
										'id_color'=>$color,
										'id_article'=>$id_article,
										'secret'=>$secret
									   ));
			
			
			if($req_update_art){
				$erreur='
					<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Mise à jour  réussie!</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
					</div>';
			}
				
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun tshirt trouvé.
				</div>';
		}
	}
	
	//----------------------Delete tshirt----------------------------------

	if(isset($_POST['sup_tshirt']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM tshirt WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Suppréssion réussie</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	

	//----------------Ajouter  categorie ----------------------------------------

	if(isset($_POST['register_categ'])){
		$erreur='';
		$nom=$_POST['nom'];
		$secret=rand(0,751454);
		$req_select=$bdd->prepare('SELECT * FROM category WHERE category=:category ');
		$req_select->execute(array('category'=>$nom));
		$n=$req_select->rowCount();

		if ($n==0) {
			$req_insert=$bdd->prepare('INSERT INTO category (ID, category,secret) VALUES (:ID, :category,:secret)');
			$req_insert->execute(array(
									'ID'=>NULL,
									'category'=>$nom,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Categorie ajoutée avec succés !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
					</div>';
			}
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur!</strong> Cette catégorie !!!
				</div>';
		}
	}


	//----------------modifier  categorie ----------------------------------------

	if(isset($_POST['edit_categ'])){
		$erreur='';
		if(isset($_POST['nom'])){
			$nom=$_POST['nom'];
		}else{
			$nom='';
		}
		
		$secret=$_POST['secret'];
		
			$req_insert=$bdd->prepare('UPDATE category SET category=:nom WHERE secret=:secret');
			$req_insert->execute(array(
									'nom'=>$nom,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Categorie modifiée avec succés !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de mise à jour. Veuillez réessayer !!!
					</div>';
			}
	
	}


	//---------------------Delete catégorie

	if(isset($_POST['sup_cat']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM category WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong> Catégorie supprimé avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	
	

	
	//----------------Ajouter une sous catégorie ----------------------------------------

	if(isset($_POST['register_sous_categ'])){
		$erreur='';
		$id_cat=$_POST['id_cat'];
		$nom=$_POST['nom'];
		$secret=rand(0,7777777);
		$req_select=$bdd->prepare('SELECT * FROM sous_category WHERE sous_category=:sous_category AND id_cat=:id_cat ');
		$req_select->execute(array('sous_category'=>$nom,'id_cat'=>$id_cat));
		$n=$req_select->rowCount();

		if ($n==0) {
			$req_insert=$bdd->prepare('INSERT INTO sous_category (ID, sous_category,id_cat, secret) VALUES (:ID, :sous_category,:id_cat,:secret)');
			$req_insert->execute(array(
									'ID'=>NULL,
									'sous_category'=>$nom,
									'id_cat'=>$id_cat,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Categorie ajoutée avec succés !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
					</div>';
			}
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur!</strong> Cette sous catégorie existe déjà à cette catégorie!!!
				</div>';
		}
	}

	
	

	//----------------modifier sous catégorie----------------------------------------

	if(isset($_POST['edit_sous_cat'])){
		$erreur='';
		
		if(isset($_POST['id_cat'])){
			$id_cat=$_POST['id_cat'];
		}else{
			$id_cat='';
		}

		if(isset($_POST['sous_category'])){
			$sous_category=$_POST['sous_category'];
		}else{
			$sous_category='';
		}

		$secret=$_POST['secret'];
		
			$req_insert=$bdd->prepare('UPDATE sous_category SET sous_category=:sous_category ,  id_cat=:id_cat WHERE secret=:secret');
			$req_insert->execute(array(
									'sous_category'=>$sous_category,
									'id_cat'=>$id_cat,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Sous Categorie  modifiée avec succés !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de mise à jour. Veuillez réessayer !!!
					</div>';
		   }
	}

	
	//---------------------Delete sous catégorie

	if(isset($_POST['sup_sous_cat']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM sous_category WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Sous catégorie supprimé avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	
	

	
	//----------------Ajouter une couleur ----------------------------------------

	if(isset($_POST['add_color'])){
		$erreur='';
		$code=$_POST['code'];
		$nom_color=addslashes($_POST['nom_color']);
		$secret=rand(0,9999999);
		
		$req_select=$bdd->prepare('SELECT * FROM couleur WHERE code=:code ');
		$req_select->execute(array('code'=>$code));
		$n=$req_select->rowCount();

	    if ($n>0) {
			
			$erreur='
					<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Ce couleur existe déjà!</strong>
					</div>';
			
		}
		else{
			
			$req_insert=$bdd->prepare('INSERT INTO couleur (ID, nom, code, secret) VALUES (:ID, :nom, :code, :secret)');
			$req_insert->execute(array(
									'ID'=>NULL,
									'nom'=>$nom_color,
									'code'=>$code,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Couleur ajoutée avec succés !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de l\'ajout de la couleur. Veuillez réessayer !!!
					</div>';
			}
		}
	}
	
	
	//----------------Modifier une couleur ----------------------------------------

	if(isset($_POST['edit_color'])){
		$erreur='';
		$code=$_POST['code'];
		$secret=$_POST['secret'];
		if(isset($_POST['nom_color'])){
			$nom_color=addslashes($_POST['nom_color']);
		}else{
			$nom_color='';
		}
		
			
			$req_insert=$bdd->prepare('UPDATE couleur  SET nom=:nom, code=:code WHERE secret=:secret');
			$req_insert->execute(array(
									'secret'=>$secret,
									'nom'=>$nom_color,
									'code'=>$code
									
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Mise à jour réussie !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de  mise à jour !!!
					</div>';
			}
		
	}
	//-----------------Supprimer un forum------------------
	if(isset($_POST['delete_forum']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM forum WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Forum supprimé avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	
	

	//----------------Activation forum ----------------------------------------

	if(isset($_POST['active_forum'])){
		$erreur='';
		$secret=$_POST['secret'];
		 $req_select=$bdd->prepare('SELECT * FROM forum WHERE secret=:secret');
        $req_select->execute(array('secret'=>$secret));
        $donnees=$req_select->fetch(PDO::FETCH_ASSOC);
        $activation=$donnees['activation']; 
        if($activation=='0'){
           $activation=1;
        }else{
        	$activation=0;
        }
		$req_supp=$bdd->prepare('UPDATE  forum SET activation=:activation WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret,
			                      'activation'=>$activation
			                       ));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Statut du forum modifié avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la modification de la statut. Réessayer!.
					</div>';
			}
	}

//---------------------------- Messagerie -------------------------------

	if(isset($_POST['submit_msg']))
	{
		$erreur='';
		$objet = $_POST['subject'];
		$nom= $_POST['nom'];
		$email= $_POST['email'];
		$telephone= $_POST['telephone'];
		$secret=rand(0,9999999);
		
		$message =$_POST['message'];
			
		$req_insert=$bdd->prepare('INSERT INTO messagerie (ID, interesse, nom, telephone, objet, message, type, secret, lecture, date) VALUES (:id, :inte, :nom, :tel, :obj, :msg, :typ, :secret, :lect, :dat)');
		$req_insert->execute(array(
								'id'=>NULL,
								'inte'=>$email,
								'nom'=>$nom,
								'tel'=>$telephone,
								'obj'=>$objet,
								'msg'=>$message,
								'typ'=>'reception',
								'secret'=>$secret,
								'lect'=>0,
								'dat'=>$date
							));

		if ($req_insert) {

			$headers ='From: "'.$nom.' "'.$email.''."\n"; 
			$headers .='Reply-To: '.$email.''."\n"; 
			$headers .='Content-Type: text/html; charset="iso-8859-1"'."\n"; 
			$headers .='Content-Transfer-Encoding: 8bit';
			
			if (mail($email, $objet, $message, $headers))
			{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Message envoyé avec succés. Merci !</strong>
					</div>';
			}
		}
		else
		{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Echec d\'envoi du message. Veuillez réessayer !</strong>
				</div>';
		}
	}
	


	

//-------------------------- send NEWSLETTER

	if(isset($_POST['newsletter']))
	{
		$erreur='';
		$secret=rand(0,9999999);
		
		$objet = $_POST['objet'];
		$message = $_POST['message'];
		
		$error = $_FILES['fichier']['error'];
		$fichier = $_FILES['fichier']['name'];
		
		$passage_ligne = "\n";
		$boundary = "-----=".md5(rand());
		$boundary_alt = "-----=".md5(rand());
		
		$header = "From: \"ETS THIONDY\"<no_reply@noreplay.com>".$passage_ligne;
		$header.= "Reply-to: \"ETS THIONDY\"<no_reply@noreplay.com>".$passage_ligne;
		$header.= "MIME-Version: 1.0".$passage_ligne;
		$header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
		
		$message= $message;
		
		
		
		if(!empty($fichier))
		{
			$dossier = 'files/';
			$fichier2 = basename($fichier);
			$uploadedfile = $_FILES['fichier']['tmp_name'];
		
			move_uploaded_file($uploadedfile, $dossier.$fichier2);
				
			$file_name = 'files/'.$fichier.'';

			$file_type = filetype($file_name);
			$file_size = filesize($file_name);
	
			$handle   = fopen($file_name, "r");
			$attachement = fread($handle, filesize($file_name));
			$attachement = chunk_split(base64_encode($attachement));
			fclose($handle);
		}
		else{
			$fichier='';
			$file_type='';
			$attachement='';
		}


		$req_insert=$bdd->prepare('INSERT INTO msg_newsletter (ID, objet, message, fichier, secret, date) VALUES (:idMsg, :obj, :msg, :file, :sec, :dat)');
		$req_insert->execute(array('idMsg'=>NULL,
									   'obj'=>$objet,
									   'msg'=>$message,
									   'file'=>$fichier,
									   'sec'=>$secret,
									   'dat'=>$date_time));
		
		
		$message.= "Content-Type: ".$file_type."; name=".$fichier."".$passage_ligne;
		$message.= "Content-Transfer-Encoding: base64".$passage_ligne;
		$message.= "Content-Disposition: attachment; filename=".$fichier."".$passage_ligne;
		$message.= $passage_ligne.$attachement.$passage_ligne.$passage_ligne;
		$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
		

		$e_mail = '';
		$req_= $bdd->prepare("SELECT * FROM newsletter");
		$req_->execute();
		$nb = $req_->rowCount();

		if ($nb!=0)
		{
			while($donnees_ = $req_->fetch(PDO::FETCH_ASSOC) )
			{
				$e_mail .= $donnees_['email'].','; 
			}
		}

		$All_Mails = substr($e_mail, 0,-1);
		
		if ($req_insert){
			if (mail($All_Mails, $objet, $message, $header))
			{
				$erreur ='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Message envoyer avec succés !</strong>
					</div>
					';
			}
			else
			{
				$erreur ='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Echec d\'envoi du message. Veuillez réessayer !</strong>
					</div>
					';
			}
		}
		else
		{
			$erreur ='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Echec d\'insertion du message. Veuillez réessayer !</strong>
				</div>
				';
		}
	}
	
	



//---------------------------- Messagerie -------------------------------

	if(isset($_POST['register_temoin']))
	{
		$erreur='';
		$nom= $_POST['nom'];
		$comp= $_POST['compagnie'];
		$fr_msg= $_POST['message_fr'];
		$en_msg= $_POST['message_en'];
		$secret=rand(0,9999999);
			
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		if(!empty($photo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= 'images/temoin/';
					$upload_image=$_FILES['photo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$photo);
						
				}
			}
		}
		else
		{
			$photo='user.png';
		}
		

		$req_insert=$bdd->prepare('INSERT INTO temoignage (ID, auteur, compagnie, message_fr, message_en, photo, secret, date) VALUES (:id, :aut, :comp, :fr_msg, :en_msg, :tof, :secret, :dat)');
		$req_insert->execute(array(
								'id'=>NULL,
								'aut'=>$nom,
								'comp'=>$comp,
								'fr_msg'=>$fr_msg,
								'en_msg'=>$en_msg,
								'tof'=>$photo,
								'secret'=>$secret,
								'dat'=>$date
							));

		if ($req_insert) {
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>témoignage ajouté avec succés.</strong>
				</div>';
		}
		else
		{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Echec d\'enregistrement du témoignage. Veuillez réessayer !</strong>
				</div>';
		}
	}
	





	
	//---------------- Ajouter un service ----------------------------------------

	if(isset($_POST['register_service'])){
		$erreur='';
		$titre=$_POST['titre'];
		$title=$_POST['title'];
		$en_desc=$_POST['description_en'];
		$fr_desc=$_POST['description_fr'];
		$secret=rand(0,9999999);
		
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		if(!empty($photo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= '../images/services/';
					$upload_image=$_FILES['photo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$photo);
						
				}
			}
		}
		else
		{
			$photo='user.png';
		}
		
		$req_insert=$bdd->prepare('INSERT INTO service (ID, service_fr, service_en, description_fr, description_en, icone, secret, date) VALUES (:id, :titr, :titl, :desc_fr, :desc_en, :photo, :secret, :dat)');
		$req_insert->execute(array(
								'id'=>NULL,
								'titr'=>$titre,
								'titl'=>$title,
								'desc_fr'=>$fr_desc,
								'desc_en'=>$en_desc,
								'photo'=>$photo,
								'secret'=>$secret,
								'dat'=>$date
							));
		
		
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Produit publié avec succés!</strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
				</div>';
		}
	}
	
	
	
	
	
	
	//------------------- Update service ---------------------------
	if(isset($_POST['update_service'])){
		$erreur='';
		$titre=$_POST['titre'];
		$title=$_POST['title'];
		$en_desc=$_POST['description_en'];
		$fr_desc=$_POST['description_fr'];
		$secret=$_POST['secret'];
		
		$photo=$_FILES['photo']['name'];
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM service WHERE secret=:sec');
		$req_select->execute(array('sec'=>$secret));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
		if($n>0){
			
			if(!empty($photo)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= 'images/';
						$upload_image=$_FILES['photo']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$photo);
							
					}
				}
			}
			else
			{
				$photo=$donnees['icone'];
			}
			-
			$req_update_=$bdd->prepare('UPDATE service SET service_fr=:titre, service_en=:title, description_fr=:fr_desc, description_en=:en_desc, icone=:image, date=:dat WHERE secret=:sec');
			$req_update_->execute(array(
									'titre'=>$titre,
									'title'=>$title,
									'fr_desc'=>$fr_desc,
									'en_desc'=>$en_desc,
									'image'=>$photo,
									'dat'=>$date,
									'sec'=>$secret
								));
			
			
			if($req_update_){
				$erreur='
					<div class="text-center alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Mise à jour du service réussie!</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de la modification. Veuillez réessayer !!!
					</div>';
			}
				
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun service trouvé.
				</div>';
		}
	}
	




	//Delete Service

	if(isset($_POST['delete_service']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM blog WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Article supprimé avec succés</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	
	


	
	
	
?>