-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : mar. 11 mai 2021 à 01:40
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `accord_ucad`
--

--
-- Déchargement des données de la table `accords`
--

INSERT INTO `accords` (`ID`, `titre`, `conventionnaire`, `date`, `texte`, `fichier`) VALUES
(1, 'Accord1', 'Autre', '2021-05-26 23:50:58', 'ACCORD-CADRE DE COOPERATION\r\nentre\r\nL\'UNIVERSITE CHEIKH ANTA DIOP DE DAKAR (SENEGAL)\r\net\r\n………………………\r\n\r\nVu les textes législatifs et réglementaires en matière de coopération dans les domaines de l\'Enseignement Supérieur, de la recherche scientifique et technique et de la culture entre la République du Sénégal et ………………………………………………….,\r\n\r\nL\'Université Cheikh Anta DIOP de Dakar, représentée par son Recteur le Professeur Ibrahima THIOUB en vertu des pouvoirs qui lui sont conférés,\r\n\r\nEt\r\n\r\n………………………., représentée par son …………………….., ………………………………………, en vertu des pouvoirs qui lui sont conférés,\r\n\r\nExpriment par le présent accord cadre leur intention de développer leurs liens de coopération.\r\n\r\nARTICLE 1\r\n\r\nL\'Université Cheikh Anta DIOP de Dakar et ………………. décident de collaborer dans le cadre de la recherche, de l\'enseignement et de la formation ainsi que de la diffusion des connaissances et de la culture sur les bases suivantes :\r\n\r\nélaboration et participation à des programmes de formation\r\n élaboration et participation à des programmes conjoints de recherche\r\n facilitation de l\'accès à la connaissance scientifique (échanges de documentations, publications, colloques, …)\r\néchanges d’enseignants\r\néchanges de chercheurs\r\néchanges d\'étudiants\r\néchanges de personnels techniques et administratifs en fonction des besoins spécifiques\r\npromotion et participation à toutes formes d\'échanges susceptibles de valoriser leurs établissements et leurs personnels que ce soit dans le cadre de leur fonctionnement interne ou de celui des relations avec leur environnement économique, industriel, social ou culturel.\r\n\r\nARTICLE 2\r\n\r\nLa coopération pourra porter sur l\'ensemble des champs disciplinaires communs aux deux institutions.\r\n\r\nARTICLE 3\r\n\r\nDes avenants au présent accord cadre préciseront, selon les composantes des universités et/ou des domaines disciplinaires concernés, les objectifs, les contenus, les effectifs impliqués et les modalités pédagogiques, administratives et financières de mis en œuvre des bases de coopération décrites aux articles 1 et 2. Ces mêmes avenants indiqueront également les procédures de suivi et d\'évaluation ainsi que leur périodicité.\r\n\r\nARTICLE 4\r\n\r\nL\'ensemble des informations recueillies ou échangées dans le cadre de la coopération et, notamment, lors des séjours scientifiques, ainsi que les résultats des recherches menées ou des techniques mises au point en commun ne pourront être divulguées à des tiers sans l\'autorisation de chacune des parties.\r\n\r\nARTICLE 5\r\n\r\nPour chaque projet comportant des coopérations dans le domaine de la recherche, les parties doivent assurer une protection effective et un partage équitable des droits de propriété intellectuelle.\r\nLes règles suivantes s\'appliqueront à la coopération :\r\ndans le cadre des projets de recherche, chacune des parties reste seule titulaire de tous les droits de propriété intellectuelle acquis antérieurement ou résultant de recherches indépendantes.\r\nLes résultats issus de projets non couverts par l\'alinéa précédent, menés dans les domaines scientifiques décrits dans les avenants à l\'accord et susceptibles d\'être protégés au titre de la propriété intellectuelle, feront l\'objet d\'une protection sur les bases suivantes : en cas de dépôt de brevet, les deux parties examineront ensemble les modalités de dépôt, d\'extension et de maintien des titres de propriété en fonction des apports intellectuels et financiers respectifs des deux institutions.\r\n\r\nARTICLE 6\r\n\r\nLes échanges et autres formes de coopération prévus dans cet accord seront effectués conformément à la réglementation existante dans chaque pays.\r\n\r\n\r\nARTICLE 7\r\n\r\nPour permettre la mise en œuvre des coopérations prévues aux articles 1,2 et 3 du présent accord, les deux institutions solliciteront l\'attribution de moyens relevant d\'une part, du domaine bilatéral et d\'autre part, du domaine multilatéral. Les demandes concernant le financement des projets de recherche (équipement, fonctionnement, missions et stages de formation) feront l\'objet de documents annexés présentés aux services gouvernementaux compétents et/ou aux partenaires.\r\n\r\nARTICLE 8\r\n\r\nCet accord est conclu pour une durée de cinq ans et prend effet à la date de sa signature. Il peut être dénoncé par écrit par l\'une ou l\'autre des deux parties, sous réserve d\'un préavis de six mois et sans préjudice pour les coopérations en cours. Il est renouvelable après avoir été à nouveau soumis aux autorités compétentes dans chaque institution concernée.\r\n\r\nLe présent accord pourra être modifié (ou amendé) d\'un commun accord par les parties, au terme de chaque année universitaire à la demande écrite de l\'une des parties dans les mêmes conditions que pour sa dénonciation.\r\n\r\nARTICLE 9\r\n\r\nCet accord cadre est rédigé en français en quatre exemplaires originaux et en ……………. en en quatre exemplaires originaux, chacun des exemplaires faisant également foi.\r\n\r\n\r\n\r\nFait à Dakar, le\r\n\r\nFait à ………….., le\r\nLe Recteur de l\'Université Cheikh Anta DIOP de DAKAR\r\n\r\n\r\n\r\n\r\nProfesseur Ibrahima THIOUB\r\nLe …………… de ………….\r\n\r\n\r\n\r\n\r\n\r\n……………………………………………..\r\n\r\n\r\nARTICLE 9\r\n\r\nCet accord cadre est rédigé en français en quatre exemplaires originaux, chacun des exemplaires faisant également foi.\r\n\r\n\r\n\r\nFait à Dakar, le\r\n\r\nFait à ………….., le\r\nLe Recteur de l\'Université Cheikh Anta DIOP de DAKAR\r\n\r\n\r\n\r\n\r\nProfesseur Abdou Salam SALL\r\nLe …………… de ………….\r\n\r\n\r\n\r\n\r\n\r\n……………………………………………..\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nPAGE  \r\n\r\n\r\nPAGE  5\r\n\r\n\r\n\r\n Cette formulation de l’article 9 convient dans le cas où les deux parties contractantes ne sont pas toutes francophones. En effet, les accords et avenants sont rédigés dans les langues de travail des deux parties.\r\n Cette formulation de l’article 9 convient dans le cas où les deux parties contractantes sont francophones. En effet, les accords et avenants sont rédigés dans les langues de travail des deux parties.\r\n\r\n\r\n', '0');

--
-- Déchargement des données de la table `membres`
--

INSERT INTO `membres` (`IdMembre`, `prenom`, `nom`, `profile`, `email`, `password`, `sexe`, `date_naissance`, `adresse`, `telephone`, `Faculte`, `INE`, `activation`) VALUES
(1, 'Omar', 'Ndiaye', 'default.png', 'omar.ndiaye1@uvs.edu.sn', 'be62546a3adff1a24c1f0282220b878e1d60ec6a', 'Masculin', '21/01/1994', 'Parcelles Assainies U19 V490', '771264186', 'MAI', 'N01147520151', '1'),
(2, 'Mame Cheikh', 'Gueye', 'default.png', 'mamec.gueye@uvs.edu.sn', '69c5fcebaa65b560eaf06c3fbeb481ae44b8d618', 'Masculin', '25/09/1994', 'Parcelles Assainies U20 V13', '778736960', 'MAI', 'N02049320151', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
